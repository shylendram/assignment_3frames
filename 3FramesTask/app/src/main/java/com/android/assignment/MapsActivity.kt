package com.android.assignment

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.android.assignment.adapter.PlacesAdapter
import com.beust.klaxon.*
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.location.places.GeoDataClient
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import kotlinx.android.synthetic.main.activity_maps.*
import org.jetbrains.anko.async
import org.jetbrains.anko.uiThread
import java.io.IOException
import java.net.URL

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private val TAG = "MapsActivity"
    private lateinit var map: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    private lateinit var placesAdapter: PlacesAdapter
    private lateinit var latLngFrom: LatLng
    private lateinit var latLngTo: LatLng
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private lateinit var mGeoDataClient: GeoDataClient
    private lateinit var mSettingsClient: SettingsClient
    private lateinit var mLocationSettingsRequest: LocationSettingsRequest
    private lateinit var location: Location
    private var isAutoCompleteLocation = false
    private var doubleBackToExitPressedOnce = false
    private var primaryTextSource: CharSequence? = null
    private var primaryTextDestination: CharSequence? = null
    private val boundsIndia = LatLngBounds(LatLng(23.63936, 68.14712), LatLng(28.20453, 97.34466))

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        try {
            confirm_button.visibility = View.GONE

            MapsInitializer.initialize(this)
            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
            mapFragment.getMapAsync(this)

            // Construct FusedLocationProviderClient
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

            // Construct a GeoDataClient.
            mGeoDataClient = Places.getGeoDataClient(this)

            // getting current location from last location
            mLocationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    val loc = locationResult!!.lastLocation
                    if (!isAutoCompleteLocation) {
                        location = loc
                        latLngFrom = LatLng(location.latitude, location.longitude)
//                    assignToMap()
                    }
                }
            }

            // setting time interval
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval((10 * 1000).toLong())        // 10 seconds, in milliseconds
                    .setFastestInterval((1 * 1000).toLong()) // 1 second, in milliseconds

            mSettingsClient = LocationServices.getSettingsClient(this)
            val builder = LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest)
            mLocationSettingsRequest = builder.build()

            // Adding places adapter to auto complete texts
            placesAdapter = PlacesAdapter(this, android.R.layout.simple_list_item_1, mGeoDataClient, null, boundsIndia)
            enter_from_place.setAdapter(placesAdapter)
            enter_to_place.setAdapter(placesAdapter)

            // on source location change listener
            enter_from_place.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (count > 0) {
                        cancel_from.visibility = View.VISIBLE
                    } else {
                        cancel_from.visibility = View.GONE
                        confirm_button.visibility = View.GONE
                    }
                }
            })

            // on Destination location change listener
            enter_to_place.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (count > 0) {
                        cancel_to.visibility = View.VISIBLE
                    } else {
                        cancel_to.visibility = View.GONE
                        confirm_button.visibility = View.GONE
                    }
                }
            })

            // On Source location item selection
            enter_from_place.setOnItemClickListener { parent, view, position, id ->
                //getLatLong(placesAdapter.getPlace(position))
                hideKeyboard()
                val item = placesAdapter.getItem(position)
                val placeId = item?.placeId

                primaryTextSource = item?.getPrimaryText(null)

                Log.i("Autocomplete", "Autocomplete item selected: $primaryTextSource")

                val placeResult = mGeoDataClient.getPlaceById(placeId)
                placeResult.addOnCompleteListener { task ->
                    val places = task.result;
                    val place = places.get(0)

                    isAutoCompleteLocation = true
                    latLngFrom = place.latLng

                    /* try {
                         assignToMap()
                     } catch (e: Exception) {
                         e.printStackTrace()
                         placeMarkerOnMap(latLngFrom, primaryTextSource.toString())
                     }
 */
                    places.release()
                }

                Toast.makeText(applicationContext, "Clicked: $primaryTextSource", Toast.LENGTH_SHORT).show()
            }
            cancel_from.setOnClickListener {
                enter_from_place.setText("")
            }

            // On Destination location item selected
            enter_to_place.setOnItemClickListener { parent, view, position, id ->
                //getLatLong(placesAdapter.getPlace(position))
                hideKeyboard()
                val item = placesAdapter.getItem(position)
                val placeId = item?.placeId
                primaryTextDestination = item?.getPrimaryText(null)

                Log.i("Autocomplete", "Autocomplete item selected: $primaryTextDestination")

                val placeResult = mGeoDataClient.getPlaceById(placeId)
                placeResult.addOnCompleteListener { task ->
                    val places = task.result;
                    val place = places.get(0)

                    isAutoCompleteLocation = true
                    latLngTo = place.latLng

//                    assignToMap()

                    places.release()
                }

                Toast.makeText(applicationContext, "Clicked: $primaryTextDestination", Toast.LENGTH_SHORT).show()
            }
            cancel_to.setOnClickListener {
                enter_to_place.setText("")
            }

            go_button.setOnClickListener(View.OnClickListener { v ->
                val sourceLocation = enter_from_place.text.toString()
                val destinationLocation = enter_to_place.text.toString()

                if (TextUtils.isEmpty(sourceLocation) || TextUtils.isEmpty(destinationLocation)) {
                    Toast.makeText(applicationContext, "Please enter both From and To", Toast.LENGTH_SHORT).show()
                } else {
                    assignToMap()
                }
            })

            // Once confirm redirect to Google Maps for Navigation
            confirm_button.setOnClickListener { v ->
                try {
                    val intent = Intent(android.content.Intent.ACTION_VIEW,
                            Uri.parse("http://maps.google.com/maps?saddr=" + latLngFrom.latitude + "," + latLngFrom.longitude +
                                    "&daddr=" + latLngTo.latitude + "," + latLngTo.longitude))
                    startActivity(intent)
                } catch (ex: ActivityNotFoundException) {
                    ex.printStackTrace()
                    Toast.makeText(applicationContext, "Not found Google Maps app in this device", Toast.LENGTH_SHORT).show()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        map.mapType = GoogleMap.MAP_TYPE_NORMAL
        map.uiSettings?.apply {
            isZoomControlsEnabled = false
            isCompassEnabled = true
        }
        // ask location permissions if not allowed
        setUpMap()

    }

    // pass lat lng and get route on map
    private fun assignToMap() {
        try {
            progress_bar.visibility = View.VISIBLE
            map.clear()
            // declare bounds object to fit whole route in screen
            val LatLongB = LatLngBounds.Builder()

            /*val options = MarkerOptions()
                    .position(latLngFrom)
                    .title("My Location")*/

            map.apply {
                //                placeMarkerOnMap(latLngFrom, primaryTextSource.toString())
//                placeMarkerOnMap(latLngTo, primaryTextDestination.toString())

                val markerOptionsSource = MarkerOptions().position(latLngFrom).title(primaryTextSource.toString())
                markerOptionsSource.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(resources, R.drawable.source)))
                map.addMarker(markerOptionsSource)

                val markerOptionsDestination = MarkerOptions().position(latLngTo).title(primaryTextDestination.toString())
                markerOptionsDestination.icon(BitmapDescriptorFactory.fromBitmap(
                        BitmapFactory.decodeResource(resources, R.drawable.destination)))
                map.addMarker(markerOptionsDestination)
//            moveCamera(CameraUpdateFactory.newLatLng(latLngFrom))
//            animateCamera(CameraUpdateFactory.newLatLngZoom(latLngFrom, 15f))

                val options = PolylineOptions()
                options.color(Color.RED)
                options.width(6f)

                val url = getURL(latLngFrom, latLngTo)

                async {
                    // Connect to URL, download content and convert into string asynchronously
                    val result = URL(url).readText()
                    // When API call is done, create parser and convert into JsonObject
                    val parser: com.beust.klaxon.Parser = Parser()
                    val stringBuilder = StringBuilder(result)
                    val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                    val routes = json.array<JsonObject>("routes")

                    uiThread {
                        // get to the correct element in JsonObject
                        if (routes != null && routes.size > 0) {
                            val points = routes["legs"]["steps"][0] as JsonArray<JsonObject>
                            // For every element in the JsonArray, decode the polyline string and pass all points to a List
//                val polypts = points.map { it.obj("polyline")?.string("points")!!  }

                            val polyPoints = points.flatMap { decodePoly(it.obj("polyline")?.string("points")!!) }

                            options.add(latLngFrom)
                            LatLongB.include(latLngFrom)
                            for (point in polyPoints) options.add(point)
                            options.add(latLngTo)
                            LatLongB.include(latLngTo)
                            // build bounds
                            val bounds = LatLongB.build()

                            // add polyline to the map
                            map.addPolyline(options)

                            progress_bar.visibility = View.GONE
                            // show map with route centered
                            map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

                            if (TextUtils.isEmpty(enter_from_place.text.toString()))
                                enter_from_place.setText(getAddress(latLngFrom))
                            confirm_button.visibility = View.VISIBLE
                        } else {
                            progress_bar.visibility = View.GONE
                            Toast.makeText(applicationContext, "No route found at this time.", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            progress_bar.visibility = View.GONE
            Toast.makeText(applicationContext, "Something went wrong", Toast.LENGTH_SHORT).show()
        }

    }

    // To add marker on map
    private fun placeMarkerOnMap(location: LatLng, title: String) {
        val markerOptions = MarkerOptions().position(location).title(title)
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                BitmapFactory.decodeResource(resources, R.drawable.source)))
        map.addMarker(markerOptions)
    }

    private fun getURL(from: LatLng, to: LatLng): String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val key = "key=" + resources.getString(R.string.google_maps_key)
        val params = "$origin&$dest&$sensor&$key"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        } else {
            getCurrentLocation()
        }
    }

    // After granting permission take the current location and move camera
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        try {
            if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getCurrentLocation()
                } else {
                    Toast.makeText(this@MapsActivity, R.string.permission_denied, Toast.LENGTH_LONG).show()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.isMyLocationEnabled = true
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    lastLocation = location
                    this.location = location
                    latLngFrom = LatLng(location.latitude, location.longitude)
                    placeMarkerOnMap(latLngFrom, getAddress(latLngFrom))
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngFrom, 16.0f))
                }
            }

            fusedLocationClient.lastLocation.addOnFailureListener(this) { e ->
                if (e is ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        e.startResolutionForResult(this@MapsActivity, LOCATION_PERMISSION_REQUEST_CODE)
                    } catch (sendEx: IntentSender.SendIntentException) {
                        // Ignore the error.
                    }
                }
            }

            //On My location button click fill source location with current location
            map.setOnMyLocationButtonClickListener {
                try {
                    getCurrentLocation()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                false
            }
        }
    }

    // To hide Keyboard
    private fun hideKeyboard() {
        try {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    // To decode poly line
    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(lat.toDouble() / 1E5,
                    lng.toDouble() / 1E5)
            poly.add(p)
        }

        return poly
    }

    // Get address from lat lng by Geo coding
    private fun getAddress(latLng: LatLng): String {
        val geoCoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geoCoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                /* for (i in 0 until address.maxAddressLineIndex) {
                     addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                 }*/
                addressText = address.getAddressLine(0)
                enter_from_place.setText(addressText)
                enter_from_place.clearFocus()
            }
        } catch (e: IOException) {
            Log.e(TAG, e.localizedMessage)
        }

        return addressText
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(mLocationCallback)
    }

    // Click back again to exit app
    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }

        doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({
            doubleBackToExitPressedOnce = false;
        }, 2000)
    }

}
